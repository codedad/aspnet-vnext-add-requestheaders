using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Http;

namespace asp_vnext 
{
	public class AddRequestHeaders
	{
		RequestDelegate _next;
	
		public AddRequestHeaders(RequestDelegate next) 
		{
			_next = next;
		}
		
		public async Task Invoke(HttpContext context) 
		{
			context.Request.Headers.Add("X-CustomRequestHeader","My Value Is Awesome!");
			await _next(context);
		}
	}
}